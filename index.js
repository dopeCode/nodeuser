const restify = require('restify');
const mongoose = require('mongoose');
const config = require('./config');

const server = restify.createServer();

server.use(restify.plugins.bodyParser());

server.listen(config.PORT, () => {
    mongoose.connect('mongodb://localhost:27017/authRest', { useNewUrlParser: true })
        .then(() => {
            console.log('Mongodb successfully connected.');
        })
        .catch(err => {
            console.log(`Mongodb connection error: ${err}.`);
        });
});

const db = mongoose.connection;

db.once('open', () => {
    require('./routes/user')(server);
    console.log(`Server successfully started on port ${config.PORT}.`)
});
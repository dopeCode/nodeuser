const errors = require('restify-errors');
const userModel = require('../models/user.model');
const config = require('../config');
const bcrpyt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const auth = require('../auth');

module.exports = server => {

    // Get users
    server.get('/user', async (req, res, next) => {
        try {
            const users = await userModel.find({});
            res.send(users);
            next();
        } catch (err) {
            return next(new errors.InvalidContentError(err));
        }
    });

    // Add user
    server.post('/user', async(req, res, next) => {
        if(!req.is('application/json')) return next(new errors.InvalidContentError("Expects 'application/json"));

        const { email, password } = req.body;
        const user = new userModel({
            email, password
        });

        bcrpyt.genSalt(10, (err, salt) => {
            bcrpyt.hash(user.password, salt, async(err, hash) => {
                user.password = hash;
                try {
                    const newUser = await user.save();
                    res.send(201);
                    next();
                } catch (err) {
                    return next(new errors.InternalError(err.message));
                }
            });
        });

        try {
            const newUser = await user.save();
            res.send(201);
            next();
        } catch (err) {
            return next(new errors.InternalError(err.message));
        }
    });

    // Update user
    server.put('/user/:id', async(req, res, next) => {
        if(!req.is('application/json')) return next(new errors.InvalidContentError("Expects 'application/json"));

        try {
            const user = await userModel.findOneAndUpdate({ _id: req.params.id }, req.body);
            res.send(200);
            next();
        } catch (err) {
            return next(new errors.ResourceNotFoundError(`Some error occurred when trying to update: ${err}`));
        }
    });

    // Delete user
    server.del('/user/:id', async(req, res, next) => {
        try {
            const user = await userModel.findOneAndDelete({ _id: req.params.id });
            res.send(204);
            next();
        } catch (err) {
            return next(new errors.ResourceNotFoundError(`Some error occurred when trying to update: ${err}`));
        }
    });

    //Auth user
    server.post('/user/auth', async(req, res, next) => {
        const { email, password } = req.body;
        try {
            const user = await auth.authenticate(email, password);
            const token = jwt.sign(user.toJSON(), config.JWT_SECRET, {
                expiresIn: '60m'
            });
            const { iat, exp } = jwt.decode(token);
            res.send({iat, exp, token});
            next();
        } catch (err) {
            return next(new errors.UnauthorizedError(err));
        }
    });
};
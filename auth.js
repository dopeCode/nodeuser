const bcrypt = require('bcryptjs');
const mongoose = require('mongoose');
const userModel = mongoose.model('user');

exports.authenticate = (email, password) => {
    return new Promise(async(resolve, reject) => {
        try {
            const user = await userModel.findOne({email});
            bcrypt.compare(password, user.password, (err, isMatch) => {
                if(err) throw err;
                if(isMatch) {
                    resolve(user);
                } else {
                    //throw 'Unknown error';
                    reject('Authentication failed');
                }
            });
        } catch (err) {
            reject(`Authentication failed ${err}`);
        }
    });
};